package com.zegoggles.smssync.mail;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import com.github.axet.smsgate.R;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public enum DataType {
    SMS(R.string.sms, R.string.sms_with_field, PreferenceKeys.IMAP_FOLDER, Defaults.SMS_FOLDER, PreferenceKeys.BACKUP_SMS, Defaults.SMS_BACKUP_ENABLED, PreferenceKeys.MAX_SYNCED_DATE_SMS, -1),
    MMS(R.string.mms, R.string.mms_with_field, PreferenceKeys.IMAP_FOLDER, Defaults.SMS_FOLDER, PreferenceKeys.BACKUP_MMS, Defaults.MMS_BACKUP_ENABLED, PreferenceKeys.MAX_SYNCED_DATE_MMS, Build.VERSION_CODES.ECLAIR);

    public final int resId;
    public final int withField;
    public final String backupEnabledPreference;
    public final String folderPreference;
    public final String defaultFolder;
    public final int minSdkVersion;
    public final boolean backupEnabledByDefault;
    public final String maxSyncedPreference;

    private DataType(int resId,
                     int withField,
                     String folderPreference,
                     String defaultFolder,
                     String backupEnabledPreference,
                     boolean backupEnabledByDefault,
                     String maxSyncedPreference,
                     int minSdkVersion) {
        this.resId = resId;
        this.withField = withField;
        this.folderPreference = folderPreference;
        this.defaultFolder = defaultFolder;
        this.backupEnabledPreference = backupEnabledPreference;
        this.backupEnabledByDefault = backupEnabledByDefault;
        this.maxSyncedPreference = maxSyncedPreference;
        this.minSdkVersion = minSdkVersion;
    }

    public boolean isBackupEnabled(Context context) {
        //noinspection SimplifiableIfStatement
        if (minSdkVersion > 0 && Build.VERSION.SDK_INT < minSdkVersion) {
            return false;
        } else {
            return prefs(context)
                    .getBoolean(backupEnabledPreference, backupEnabledByDefault);
        }
    }

    public void setBackupEnabled(Context context, boolean enabled) {
        prefs(context)
                .edit()
                .putBoolean(backupEnabledPreference, enabled)
                .commit();
    }

    public String getFolder(Context context) {
        return prefs(context).getString(folderPreference, defaultFolder);
    }


    /**
     * @param context the context
     * @return returns the last synced date in msecs (epoch)
     */
    public long getMaxSyncedDate(Context context) {
        long maxSynced = prefs(context).getLong(maxSyncedPreference, Defaults.MAX_SYNCED_DATE);
        if (this == MMS && maxSynced > 0) {
            return maxSynced * 1000L;
        } else {
            return maxSynced;
        }
    }

    public boolean setMaxSyncedDate(Context context, long max) {
        return prefs(context).edit().putLong(maxSyncedPreference, max).commit();
    }

    private SharedPreferences prefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static EnumSet<DataType> enabled(Context context) {
        List<DataType> enabledTypes = new ArrayList<DataType>();
        for (DataType t : values()) {
            if (t.isBackupEnabled(context)) {
                enabledTypes.add(t);
            }
        }
        return enabledTypes.isEmpty() ? EnumSet.noneOf(DataType.class) : EnumSet.copyOf(enabledTypes);
    }

    public static long getMostRecentSyncedDate(Context ctx) {
        return Math.max(SMS.getMaxSyncedDate(ctx), MMS.getMaxSyncedDate(ctx));
    }

    public static void clearLastSyncData(Context ctx) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        for (DataType type : values()) {
            editor.remove(type.maxSyncedPreference);
        }
        editor.commit();
    }

    public static class PreferenceKeys {
        public static final String IMAP_FOLDER = "imap_folder";

        public static final String BACKUP_SMS = "backup_sms";
        public static final String BACKUP_MMS = "backup_mms";

        public static final String MAX_SYNCED_DATE_SMS = "max_synced_date";
        public static final String MAX_SYNCED_DATE_MMS = "max_synced_date_mms";
        public static final String MAX_SYNCED_DATE_CALLLOG = "max_synced_date_calllog";
        public static final String MAX_SYNCED_DATE_WHATSAPP = "max_synced_date_whatsapp";
    }

    /**
     * Defaults for various settings
     */
    public static class Defaults {
        public static final long MAX_SYNCED_DATE = -1;
        public static final String SMS_FOLDER = "Inbox";

        public static final boolean SMS_BACKUP_ENABLED = true;
        public static final boolean MMS_BACKUP_ENABLED = true;

    }
}
