package com.zegoggles.smssync.activity;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.Storage;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.SmsBackupService;
import com.zegoggles.smssync.service.UserCanceled;
import com.zegoggles.smssync.service.state.BackupState;
import com.zegoggles.smssync.service.state.SmsSyncState;
import com.zegoggles.smssync.service.state.State;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

public class StatusPreference extends Preference implements View.OnClickListener {
    private Button mBackupButton;

    private ImageView mStatusIcon;

    private TextView mStatusLabel;
    private TextView mSyncDetailsLabel;

    private ProgressBar mProgressBar;
    private SMSGateFragment mainActivity;

    public StatusPreference(SMSGateFragment mainActivity) {
        super(mainActivity.getActivity());
        this.mainActivity = mainActivity;

        setLayoutResource(R.layout.status);
        setWidgetLayoutResource(R.layout.status);

        setSelectable(false);
        setOrder(0);
    }

    @Override
    public void onClick(View v) {
        if (v == mBackupButton) {
            if (!SmsBackupService.isServiceWorking()) {
                if (LOCAL_LOGV) Log.v(TAG, "user requested sync");
                mainActivity.performAction(SMSGateFragment.Actions.Backup);
            } else {
                if (LOCAL_LOGV) Log.v(TAG, "user requested cancel");
                // Sync button will be restored on next status update.
                mBackupButton.setText(R.string.ui_sync_button_label_canceling);
                mBackupButton.setEnabled(false);
                App.bus.post(new UserCanceled());
            }
        }
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        mBackupButton = (Button) holder.findViewById(R.id.sync_button);
        mBackupButton.setOnClickListener(this);

        mStatusIcon = (ImageView) holder.findViewById(R.id.status_icon);
        mStatusLabel = (TextView) holder.findViewById(R.id.status_label);
        View mSyncDetails = holder.findViewById(R.id.details_sync);
        mSyncDetailsLabel = (TextView) mSyncDetails.findViewById(R.id.details_sync_label);
        mProgressBar = (ProgressBar) mSyncDetails.findViewById(R.id.details_sync_progress);

        idle();
    }

    @Subscribe
    public void backupStateChanged(final BackupState newState) {
        if (App.LOCAL_LOGV) Log.v(TAG, "backupStateChanged:" + newState);
        if (newState.backupType.isBackground())
            return;
        if(mProgressBar == null) // called before onBind()
            return;

        stateChanged(newState);

        switch (newState.state) {
            case FINISHED_REPLYSMS:
            case FINISHED_BACKUP:
                finishedBackup(newState);
                break;
            case REPLYSMS:
                mBackupButton.setText(R.string.ui_sync_button_label_syncing);
                mStatusLabel.setText("Reply SMS");
                mSyncDetailsLabel.setText(newState.getNotificationLabel(getContext().getResources()));
                mProgressBar.setIndeterminate(false);
                mProgressBar.setProgress(newState.currentSyncedItems);
                mProgressBar.setMax(newState.itemsToSync);
                break;
            case BACKUP:
                mBackupButton.setText(R.string.ui_sync_button_label_syncing);
                mStatusLabel.setText(R.string.status_backup);
                mSyncDetailsLabel.setText(newState.getNotificationLabel(getContext().getResources()));
                mProgressBar.setIndeterminate(false);
                mProgressBar.setProgress(newState.currentSyncedItems);
                mProgressBar.setMax(newState.itemsToSync);
                break;
            case CANCELED_BACKUP:
                mStatusLabel.setText(R.string.status_canceled);

                mSyncDetailsLabel.setText(getContext().getString(R.string.status_canceled_details,
                        newState.currentSyncedItems,
                        newState.itemsToSync));
                break;
        }
    }

    private void authFailed() {
        mStatusLabel.setText(R.string.status_auth_failure);

        if (new AuthPreferences(getContext()).useXOAuth()) {
            mSyncDetailsLabel.setText(R.string.status_auth_failure_details_xoauth);
        } else {
            mSyncDetailsLabel.setText(R.string.status_auth_failure_details_plain);
        }
    }

    private void calc() {
        mStatusLabel.setText(R.string.status_working);
        mSyncDetailsLabel.setText(R.string.status_calc_details);
        mProgressBar.setIndeterminate(true);
    }

    private void finishedBackup(BackupState state) {
        int backedUpCount = state.currentSyncedItems;
        String text = null;
        if (backedUpCount == new Preferences(getContext()).getMaxItemsPerSync()) {
            text = getContext().getString(R.string.status_backup_done_details_max_per_sync, backedUpCount);
        } else if (backedUpCount > 0) {
            text = getContext().getResources().getQuantityString(R.plurals.status_backup_done_details, backedUpCount,
                    backedUpCount);
        } else if (backedUpCount == 0) {
            text = getContext().getString(R.string.status_backup_done_details_noitems);
        }
        mSyncDetailsLabel.setText(text);
        mStatusLabel.setText(R.string.status_done);
        mStatusLabel.setTextColor(getContext().getResources().getColor(R.color.status_done));
    }

    private void idle() {
        long last = DataType.getMostRecentSyncedDate(getContext());
        if (Storage.isEnabled(getContext()))
            last = Math.max(last, Storage.getLastSyncDate(getContext()));
        mSyncDetailsLabel.setText(mainActivity.getLastSyncText(last));
        mStatusLabel.setText(R.string.status_idle);
    }

    private void stateChanged(State state) {
        setViewAttributes(state.state);
        switch (state.state) {
            case INITIAL:
                idle();
                break;
            case LOGIN:
                mStatusLabel.setText(R.string.status_working);
                mSyncDetailsLabel.setText(R.string.status_login_details);
                mProgressBar.setIndeterminate(true);
                break;
            case CALC:
                calc();
                break;
            case ERROR:
                if (state.isAuthException()) {
                    authFailed();
                } else {
                    final String errorMessage = state.getErrorMessage(getContext().getResources());
                    mStatusLabel.setText(R.string.status_unknown_error);
                    mSyncDetailsLabel.setText(getContext().getString(R.string.status_unknown_error_details,
                            errorMessage == null ? "N/A" : errorMessage));
                }
                break;
        }
    }

    private void setViewAttributes(final SmsSyncState state) {
        switch (state) {
            case LOGIN:
            case CALC:
            case BACKUP:
                mStatusLabel.setTextColor(getContext().getResources().getColor(R.color.status_sync));
                mStatusIcon.setImageResource(R.drawable.ic_syncing);
                break;
            case ERROR:
                mProgressBar.setProgress(0);
                mProgressBar.setIndeterminate(false);
                mStatusLabel.setTextColor(getContext().getResources().getColor(R.color.status_error));
                mStatusIcon.setImageResource(R.drawable.ic_error);
                setButtonsToDefault();
                break;
            default:
                mProgressBar.setProgress(0);
                mProgressBar.setIndeterminate(false);
                mStatusLabel.setTextColor(getContext().getResources().getColor(R.color.status_idle));
                mStatusIcon.setImageResource(R.drawable.ic_idle);
                setButtonsToDefault();
                break;
        }
    }

    private void setButtonsToDefault() {
        mBackupButton.setEnabled(true);
        mBackupButton.setText(R.string.ui_sync_button_label_idle);
    }
}
