package com.github.axet.smsgate.services;

import android.Manifest;
import android.content.Context;

import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.services.WifiKeepService;

import java.io.File;

public class OnExternalReceiver extends com.github.axet.androidlibrary.services.OnExternalReceiver {
    @Override
    public void onBootReceived(Context context) {
        OnBootReceiver.start(context);
    }
}
