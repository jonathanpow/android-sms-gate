package com.github.axet.smsgate.app;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// API19+ writing / deleting sms requires to be default SMS app. Use DefaultSMSPreferenceCompat
public class SmsStorage {
    public static final String ID = BaseColumns._ID;
    public static final String DESC = "DESC";

    ContentResolver resolver;

    public static HashMap<String, Message> getMessagesFromIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return null;

        Object[] pdusObj = (Object[]) bundle.get("pdus");

        SmsMessage[] messages = new SmsMessage[pdusObj.length];

        for (int i = 0; i < messages.length; i++) {
            if (Build.VERSION.SDK_INT >= 23) {
                String format = intent.getStringExtra("format");
                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
            } else {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
            }
        }

        HashMap<String, Message> map = new HashMap<>();
        for (SmsMessage msg : messages) {
            String id = msg.getOriginatingAddress() + " " + msg.getTimestampMillis();
            Message m = map.get(id);
            if (m != null) {
                m.body += msg.getMessageBody();
                map.put(id, m);
            } else {
                map.put(id, new InboxMessage(msg.getOriginatingAddress(), msg.getMessageBody()));
            }
        }
        return map;
    }

    public static Map<String, String> getMessageMap(Cursor cursor) {
        final String[] columns = cursor.getColumnNames();
        final Map<String, String> map = new HashMap<>(columns.length);
        for (String column : columns) {
            String value;
            try {
                final int index = cursor.getColumnIndex(column);
                if (index != -1) {
                    value = cursor.getString(index);
                } else {
                    continue;
                }
            } catch (SQLiteException ignored) { // this can happen in case of BLOBS in the DB column type checking is API level >= 11
                value = "[BLOB]";
            }
            map.put(column, value);
        }
        return map;
    }

    public static Message getMessage(Cursor cursor) {
        Message m = new Message();
        m.id = cursor.getLong(cursor.getColumnIndex(ID));
        m.type = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.TYPE));
        m.date = cursor.getLong(cursor.getColumnIndex(Telephony.Sms.DATE));
        m.phone = cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS));
        m.body = cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY));
        m.thread = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.THREAD_ID));
        m.read = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.READ)) == 1;
        return m;
    }

    public static String querySort(String sort, int off, int max) {
        if (max > 0)
            return sort + " LIMIT " + max + (off >= 0 ? " OFFSET " + off : "");
        else
            return sort;
    }

    public static class Message {
        public long id;
        public long date;
        public int type; // Telephony.Sms.TYPE and Telephony.TextBasedSmsColumns.MESSAGE_TYPE_ ...
        public String phone; // Telephony.Sms.ADDRESS
        public String body;
        public int thread; // Telephony.Sms.THREAD_ID
        public boolean read; // seen?

        public Message() {
        }

        public Message(String p, String b) {
            this.phone = p;
            this.body = b;
        }
    }

    public static class InboxMessage extends Message {
        public InboxMessage(String p, String b) {
            super(p, b);
            type = Telephony.TextBasedSmsColumns.MESSAGE_TYPE_INBOX;
        }
    }

    public static class SentMessage extends Message {
        public SentMessage(String p, String b) {
            super(p, b);
            type = Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT;
        }
    }

    public SmsStorage(Context context) {
        resolver = context.getContentResolver();
    }

    public long add(Message m) {
        long date;
        if (m.date == 0)
            date = System.currentTimeMillis();
        else
            date = m.date;
        ContentValues values = new ContentValues();
        values.put(Telephony.Sms.ADDRESS, m.phone);
        values.put(Telephony.Sms.BODY, m.body);
        Uri uri;
        if (m instanceof InboxMessage)
            uri = Telephony.Sms.Inbox.CONTENT_URI;
        else if (m instanceof SentMessage)
            uri = Telephony.Sms.Sent.CONTENT_URI;
        else
            throw new RuntimeException("unknown type");
        values.put(Telephony.Sms.TYPE, m.type);
        values.put(Telephony.Sms.PROTOCOL, 0);
        values.put(Telephony.Sms.SERVICE_CENTER, "");
        values.put(Telephony.Sms.DATE, date);
        values.put(Telephony.Sms.STATUS, Telephony.TextBasedSmsColumns.STATUS_NONE);
        values.put(Telephony.Sms.THREAD_ID, m.thread);
        values.put(Telephony.Sms.READ, m.read ? 1 : 0);
        Uri id = resolver.insert(uri, values);
        return ContentUris.parseId(id);
    }

    public Cursor query(long date, int type, String sort, int off, int max) {
        return resolver.query(Telephony.Sms.CONTENT_URI, null,
                String.format("%s > ? and %s == ?", Telephony.Sms.DATE, Telephony.Sms.TYPE),
                new String[]{String.valueOf(date), String.valueOf(type)}, querySort(sort, off, max));
    }

    public Cursor queryAfter(long date, String sort, int off, int max) {
        return resolver.query(Telephony.Sms.CONTENT_URI, null,
                String.format("%s > ?", Telephony.Sms.DATE),
                new String[]{String.valueOf(date)}, querySort(sort, off, max));
    }

    public Cursor queryFolder(int type, String sort, int off, int max) {
        return resolver.query(Telephony.Sms.CONTENT_URI, null,
                String.format("%s == ?", Telephony.Sms.TYPE),
                new String[]{String.valueOf(type)}, querySort(sort, off, max));
    }

    public void delete(long id) {
        resolver.delete(Uri.parse(Telephony.Sms.CONTENT_URI + "/" + id), null, null);
    }

    public void updateAllThreads() {
        // thread dates + states might be wrong, we need to force a full update
        // unfortunately there's no direct way to do that in the SDK, but passing a
        // negative conversation id to delete should to the trick
        resolver.delete(Uri.parse(Telephony.Sms.Conversations.CONTENT_URI + "/-1"), null, null);
    }

    public boolean exists(long date, String phone) { // just assume equality on date+address+type
        Cursor c = resolver.query(Telephony.Sms.CONTENT_URI, new String[]{ID},
                String.format("%s = ? AND %s = ?", Telephony.Sms.DATE, Telephony.Sms.ADDRESS),
                new String[]{String.valueOf(date), phone}, null);
        boolean exists = false;
        if (c != null) {
            exists = c.getCount() > 0;
            c.close();
        }
        return exists;
    }

    public ArrayList<Long> deleteThread(int thread) {
        Cursor c = resolver.query(Telephony.Sms.CONTENT_URI, new String[]{ID, Telephony.Sms.THREAD_ID},
                Telephony.Sms.THREAD_ID + " = ?",
                new String[]{Integer.toString(thread)}, null);
        if (c != null) {
            ArrayList<Long> ids = new ArrayList<>();
            while (c.moveToNext()) {
                long id = c.getLong(0);
                long threadId = c.getLong(1);
                if (threadId == thread) {
                    delete(id);
                    ids.add(id);
                }
            }
            c.close();
            if (ids.isEmpty())
                return null;
            return ids;
        }
        return null;
    }

    public ArrayList<Long> deleteOld(long date) {
        Cursor c = resolver.query(Telephony.Sms.CONTENT_URI, new String[]{ID},
                Telephony.Sms.DATE + " < ?",
                new String[]{Long.toString(date)}, null);
        if (c != null) {
            ArrayList<Long> ids = new ArrayList<>();
            while (c.moveToNext()) {
                long id = c.getLong(0);
                delete(id);
                ids.add(id);
            }
            c.close();
            if (ids.isEmpty())
                return null;
            return ids;
        }
        return null;
    }
}
