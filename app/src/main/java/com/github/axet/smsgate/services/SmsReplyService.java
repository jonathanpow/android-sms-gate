/* Copyright (c) 2009 Christoph Studer <chstuder@gmail.com>
 * Copyright (c) 2010 Jan Berkel <jan.berkel@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.axet.smsgate.services;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.PushReceiver;
import com.fsck.k9.mail.Pusher;
import com.fsck.k9.mail.internet.BinaryTempFileBody;
import com.fsck.k9.mail.power.TracingPowerManager;
import com.fsck.k9.mail.store.imap.XOAuth2AuthenticationFailedException;
import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.smsgate.R;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.auth.TokenRefreshException;
import com.zegoggles.smssync.auth.TokenRefresher;
import com.zegoggles.smssync.mail.BackupImapStore;
import com.zegoggles.smssync.mail.BackupStoreConfig;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.Alarms;
import com.zegoggles.smssync.service.BackupConfig;
import com.zegoggles.smssync.service.BackupType;
import com.zegoggles.smssync.service.ServiceBase;
import com.zegoggles.smssync.service.SleepService;
import com.zegoggles.smssync.service.SmsBackupService;
import com.zegoggles.smssync.service.exception.BackupDisabledException;
import com.zegoggles.smssync.service.exception.ConnectivityException;
import com.zegoggles.smssync.service.exception.NoConnectionException;
import com.zegoggles.smssync.service.exception.RequiresBackgroundDataException;
import com.zegoggles.smssync.service.exception.RequiresLoginException;
import com.zegoggles.smssync.service.exception.RequiresWifiException;
import com.zegoggles.smssync.service.state.ReplyState;
import com.zegoggles.smssync.service.state.SmsSyncState;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;
import static com.zegoggles.smssync.mail.DataType.SMS;
import static com.zegoggles.smssync.service.BackupType.MANUAL;
import static com.zegoggles.smssync.service.BackupType.REPLY;
import static com.zegoggles.smssync.service.BackupType.UNKNOWN;
import static com.zegoggles.smssync.service.state.SmsSyncState.ERROR;
import static com.zegoggles.smssync.service.state.SmsSyncState.FINISHED_BACKUP;
import static com.zegoggles.smssync.service.state.SmsSyncState.INITIAL;

public class SmsReplyService extends ServiceBase {
    public static int NOTIFICATION_ICON = 202;

    @Nullable
    private SmsReplyService service;
    @NotNull
    private ReplyState mState = new ReplyState();
    Handler handler;

    Pusher pusher;
    Thread thread;
    TokenRefresher tokenRefresher;
    OptimizationPreferenceCompat.NotificationIcon icon;

    BackupImapStore imap;
    PushReceiver receiver = new PushReceiver() {
        @Override
        public Context getContext() {
            return SmsReplyService.this;
        }

        @Override
        public void syncFolder(Folder folder) {
        }

        @Override
        public void messagesArrived(Folder folder, List<Message> list) {
            start(getContext());
        }

        @Override
        public void messagesFlagsChanged(Folder folder, List<Message> list) {
        }

        @Override
        public void messagesRemoved(Folder folder, List<Message> list) {
        }

        @Override
        public String getPushState(String s) {
            return null;
        }

        @Override
        public void pushError(String s, Exception e) {
        }

        @Override
        public void setPushActive(String s, boolean b) {
        }

        @Override
        public void sleep(TracingPowerManager.TracingWakeLock tracingWakeLock, long l) {
            SleepService.sleep(getContext(), l, tracingWakeLock, 60000);
        }

        @Override
        public void authenticationFailed() {
        }
    };

    public static void start(Context context) {
        Intent intent = new Intent(context, SmsReplyService.class);
        OptimizationPreferenceCompat.startService(context, intent);
        scheduleReplyBackup(context);
    }

    public static void startIfEnabled(Context context) {
        if (SmsBackupService.shouldSchedule(context))
            start(context);
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, SmsReplyService.class);
        context.stopService(intent);
        Alarms a = new Alarms(context);
        AlarmManager.cancel(context, a.createPendingIntent(context, UNKNOWN, SmsReplyService.class));
    }

    public static long scheduleReplyBackup(Context context) {
        Alarms a = new Alarms(context);
        return a.scheduleBackup(new Preferences(context).getRegularTimeoutSecs(), REPLY, false, SmsReplyService.class);
    }

    public static class ReplyImapStore extends BackupImapStore {
        public ReplyImapStore(final Context context, final String uri) throws MessagingException {
            super(context, new ReplyStoreConfig(uri));
        }
    }

    public static class ReplyStoreConfig extends BackupStoreConfig {
        public ReplyStoreConfig(String uri) {
            super(uri);
        }

        @Override
        public boolean allowRemoteSearch() {
            return true;
        }

        @Override
        public boolean isRemoteSearchFullText() {
            return true;
        }
    }

    @Override
    @NotNull
    public ReplyState getState() {
        return mState;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "ServiceReply create");

        service = this;

        icon = new OptimizationPreferenceCompat.NotificationIcon(service, NOTIFICATION_ICON) {
            @Override
            public void updateIcon() {
                updateIcon(null); // we do not need two icons on low API phones
            }

            @Override
            public boolean isOptimization() {
                return Build.VERSION.SDK_INT >= 26 && context.getApplicationInfo().targetSdkVersion >= 26; // show double icons for API26+
            }

            @Override
            public Notification build(Intent intent) {
                return new OptimizationPreferenceCompat.PersistentIconBuilder(context).setWhen(notification)
                        .create(R.style.AppThemeDark, new NotificationChannelCompat(context, "status", "ReplyService", NotificationManagerCompat.IMPORTANCE_LOW))
                        .setText("ReplyService")
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification).build();
            }
        };
        icon.create();

        SmsBackupService.clearTempFiles(this);
        BinaryTempFileBody.setTempDirectory(getCacheDir());

        acquireLocks();

        start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        releaseLocks();

        Log.v(TAG, "ServiceReply destroy");

        if (LOCAL_LOGV) Log.v(TAG, "SmsReplyService#onDestroy(state=" + getState() + ")");
        service = null;
        stop();

        icon.close();
    }

    void connect() {
        try {
            tokenRefresher = new TokenRefresher(service, new AuthPreferences(this));
            if (imap != null)
                imap.closeFolders();
            imap = getBackupImapStore();
            imap.checkSettings();
            if (pusher != null)
                pusher.stop();
            pusher = imap.getPusher(receiver);
            pusher.start(Arrays.asList(SMS.getFolder(this)));
        } catch (XOAuth2AuthenticationFailedException e) {
            handleAuthError(e);
            return;
        } catch (AuthenticationFailedException e) {
            Log.v(TAG, e.toString());
            return;
        } catch (MessagingException e) {
            Log.v(TAG, "connect", e);
            transition(ERROR, e);
        }
    }

    @Override
    public BackupImapStore getBackupImapStore() throws MessagingException {
        final String uri = getAuthPreferences().getStoreUri();
        if (!BackupImapStore.isValidUri(uri)) {
            throw new MessagingException("No valid IMAP URI: " + uri);
        }
        return new ReplyImapStore(getApplicationContext(), uri);
    }


    private ReplyState handleAuthError(XOAuth2AuthenticationFailedException e) {
        if (e.getStatus() == 400) {
            Log.d(TAG, "need to perform xoauth2 token refresh");
            try {
                tokenRefresher.refreshOAuth2Token();
                // we got a new token, let's retry one more time - we need to pass in a new store object
                // since the auth params on it are immutable
                connect();
            } catch (TokenRefreshException refreshException) {
                Log.w(TAG, refreshException);
            }
        } else {
            Log.w(TAG, "unexpected xoauth status code " + e.getStatus());
        }
        return transition(ERROR, e);
    }

    synchronized void start() {
        if (handler != null)
            return;

        handler = new Handler();

        // stickToast();

        if (thread != null) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                connect();
            }
        });
        thread.start();
    }

    synchronized void stickToast() {
        if (handler == null)
            return;

        Log.d(TAG, "ShowToast");
        showToast();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stickToast();
            }
        }, 3000);
    }

    synchronized void stop() {
        handler = null;
    }

    void showToast() {
        notifyAboutBackup(mState);
    }

    @Override
    protected void handleIntent(final Intent intent) {
        start();

        // in case if app get killed. schedule next
        scheduleNextBackup();

        if (intent == null) return; // NB: should not happen with START_NOT_STICKY
        final BackupType backupType = BackupType.fromIntent(intent);
        if (LOCAL_LOGV) Log.v(TAG, "handleIntent(" + intent +
                ", " + (intent.getExtras() == null ? "null" : intent.getExtras().keySet()) +
                ", type=" + backupType + ")");

        appLog(R.string.app_log_backup_requested, getString(backupType.resId));

        // Only start a backup if there's no other operation going on at this time.
        if (!isWorking() && !isServiceWorking()) {
            backup(backupType, intent.getBooleanExtra(Consts.KEY_SKIP_MESSAGES, false));
        } else {
            appLog(R.string.app_log_skip_backup_already_running);
        }
    }

    private void backup(BackupType backupType, boolean skip) {
        try {
            // set initial state
            mState = new ReplyState(INITIAL, 0, 0, backupType, null, null);
            EnumSet<DataType> enabledTypes = getEnabledBackupTypes();
            if (!skip) {
                checkCredentials();
                checkBackgroundDataSettings(backupType);
                checkConnectivity();
            }

            appLog(R.string.app_log_start_backup, backupType);

            SmsReplyTask r = new SmsReplyTask(this, getPreferences(), new TokenRefresher(service, new AuthPreferences(this)));
            r.execute(getBackupConfig(backupType, enabledTypes, getBackupImapStore(), skip));
        } catch (MessagingException e) {
            Log.w(TAG, e);
            moveToState(mState.transition(ERROR, e));
        } catch (RequiresBackgroundDataException e) {
            moveToState(mState.transition(ERROR, e));
        } catch (ConnectivityException e) {
            moveToState(mState.transition(ERROR, e));
        } catch (RequiresLoginException e) {
            appLog(R.string.app_log_missing_credentials);
            moveToState(mState.transition(ERROR, e));
        } catch (BackupDisabledException e) {
            moveToState(mState.transition(FINISHED_BACKUP, e));
        }
    }

    private BackupConfig getBackupConfig(BackupType backupType,
                                         EnumSet<DataType> enabledTypes,
                                         BackupImapStore imapStore,
                                         boolean skip) {
        return new BackupConfig(
                imapStore,
                0,
                skip,
                getPreferences().getMaxItemsPerSync(),
                backupType,
                enabledTypes,
                getPreferences().isAppLogDebug()
        );
    }

    private EnumSet<DataType> getEnabledBackupTypes() throws BackupDisabledException {
        EnumSet<DataType> dataTypes = DataType.enabled(this);
        if (dataTypes.isEmpty()) {
            throw new BackupDisabledException();
        }
        return dataTypes;
    }

    private void checkCredentials() throws RequiresLoginException {
        if (!getAuthPreferences().isLoginInformationSet()) {
            throw new RequiresLoginException();
        }
    }

    private void checkBackgroundDataSettings(BackupType backupType) throws RequiresBackgroundDataException {
        if (backupType.isBackground() && !getConnectivityManager().getBackgroundDataSetting()) {
            throw new RequiresBackgroundDataException();
        }
    }

    private void checkConnectivity() throws ConnectivityException {
        NetworkInfo active = getConnectivityManager().getActiveNetworkInfo();
        if (active == null || !active.isConnectedOrConnecting()) {
            throw new NoConnectionException();
        }
        if (getPreferences().isWifiOnly() && isBackgroundTask() && !isConnectedViaWifi()) {
            throw new RequiresWifiException();
        }
    }

    private void moveToState(ReplyState state) {
        backupStateChanged(state);
        App.bus.post(state);
    }

    @Override
    protected boolean isBackgroundTask() {
        return mState.backupType.isBackground();
    }

    @Produce
    public ReplyState produceLastState() {
        return mState;
    }

    @Subscribe
    public void backupStateChanged(ReplyState state) {
        if (mState == state) return;

        mState = state;
        if (mState.isInitialState()) return;

        if (state.isError()) {
            handleErrorState(state);
        }

        if (state.isRunning()) {
            if (state.backupType == MANUAL) {
                notifyAboutBackup(state);
            }
        } else {
            appLogDebug(state.toString());
            appLog(state.isCanceled() ? R.string.app_log_backup_canceled : R.string.app_log_backup_finished);

            Log.d(TAG, "scheduling next backup");
            scheduleNextBackup();
        }
    }

    private void handleErrorState(ReplyState state) {
        if (state.isAuthException()) {
            appLog(R.string.app_log_backup_failed_authentication, state.getDetailedErrorMessage(getResources()));

            if (shouldNotifyUser(state)) {
                notifyUser(android.R.drawable.stat_sys_warning,
                        getString(R.string.notification_auth_failure),
                        getString(getAuthPreferences().useXOAuth() ? R.string.status_auth_failure_details_xoauth : R.string.status_auth_failure_details_plain));
            }
        } else if (state.isConnectivityError()) {
            appLog(R.string.app_log_backup_failed_connectivity, state.getDetailedErrorMessage(getResources()));
        } else {
            appLog(R.string.app_log_backup_failed_general_error, state.getDetailedErrorMessage(getResources()));

            if (shouldNotifyUser(state)) {
                notifyUser(android.R.drawable.stat_sys_warning,
                        getString(R.string.notification_general_error),
                        state.getErrorMessage(getResources()));
            }
        }
    }

    private boolean shouldNotifyUser(ReplyState state) {
        return state.backupType == MANUAL ||
                (getPreferences().isNotificationEnabled() && !state.isConnectivityError());
    }

    private void notifyAboutBackup(ReplyState state) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher_notification);
        builder.setTicker(getString(R.string.status_backup));
        builder.setWhen(System.currentTimeMillis());
        builder.setOngoing(true);
        if (Build.VERSION.SDK_INT >= 16)
            builder.setPriority(Notification.PRIORITY_LOW);
        builder.setContentIntent(getPendingIntent());
        builder.setContentTitle(getString(R.string.status_backup));
        builder.setContentText(state.getNotificationLabel(getResources()));
        NotificationChannelCompat channel = new NotificationChannelCompat(this, "reply", "ReplyService", NotificationManagerCompat.IMPORTANCE_LOW);
        channel.apply(builder);
        notification = builder.build();
        NotificationChannelCompat.setChannelId(notification, channel.channelId);
        startForeground(BACKUP_ID, notification);
    }

    private void scheduleNextBackup() {
        final long nextSync = scheduleReplyBackup(this);
        if (nextSync >= 0)
            appLog(R.string.app_log_scheduled_next_sync, DateFormat.format("kk:mm", new Date(nextSync)));
        else
            appLog(R.string.app_log_no_next_sync);
    }

    protected void notifyUser(int icon, String title, String text) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(icon);
        builder.setTicker(getString(R.string.app_name));
        builder.setWhen(System.currentTimeMillis());
        builder.setOnlyAlertOnce(true);
        builder.setAutoCancel(true);
        builder.setContentIntent(getPendingIntent());
        builder.setContentTitle(title);
        builder.setContentText(text);
        NotificationChannelCompat channel = new NotificationChannelCompat(this, "reply", "ReplyService", NotificationManagerCompat.IMPORTANCE_LOW);
        channel.apply(builder);
        Notification n = builder.build();
        NotificationChannelCompat.setChannelId(n, channel.channelId);
        getNotifier().notify(BACKUP_USER, n);
    }

    protected Alarms getAlarms() {
        return new Alarms(this);
    }

    public boolean isServiceWorking() {
        return service != null && service.isWorking();
    }

    public ReplyState transition(SmsSyncState newState, Exception e) {
        return mState.transition(newState, e);
    }
}
