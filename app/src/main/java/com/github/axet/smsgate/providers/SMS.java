package com.github.axet.smsgate.providers;

import android.content.Context;
import android.os.Build;
import android.telephony.SmsManager;

import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.mediatek.SmsManagerMT;

import java.util.ArrayList;

/**
 * https://github.com/shutoff/ugona.net/blob/master/src/main/java/net/ugona/plus/Sms.java
 */
public class SMS {
    public static String filterPhone(String phone) {
        phone = phone.replaceAll(" ", "");
        phone = phone.replaceAll("-", "");
        phone = phone.replaceAll("\\(", "");
        phone = phone.replaceAll("\\)", "");
        return phone;
    }

    public static void send(int simID, String phone, String msg) {
        phone = filterPhone(phone);
        if (Build.VERSION.SDK_INT >= 22) {
            SmsManager sm = SmsManager.getSmsManagerForSubscriptionId(simID);
            ArrayList<String> parts = sm.divideMessage(msg);
            if (parts.size() > 1) {
                sm.sendMultipartTextMessage(phone, null, parts, null, null);
            } else {
                sm.sendTextMessage(phone, null, msg, null, null);
            }
            return;
        }

        try {
            SmsManagerMT mt = new SmsManagerMT();
            ArrayList<String> parts = mt.divideMessage(msg);
            if (parts.size() > 1) {
                mt.sendMultipartTextMessage(phone, null, parts, null, null, simID);
            } else {
                mt.sendTextMessage(phone, null, msg, null, null, simID);
            }
            return; // metdatek phone
        } catch (Throwable ignore) {
        }
    }

    public static void send(Context context, int simID, String phone, String msg) {
        send(simID, phone, msg);
        SmsStorage storage = new SmsStorage(context);
        storage.add(new SmsStorage.SentMessage(phone, msg));
    }

    public static void send(String phone, String msg) {
        phone = filterPhone(phone);
        SmsManager sm = SmsManager.getDefault();
        ArrayList<String> parts = sm.divideMessage(msg);
        if (parts.size() > 1) {
            sm.sendMultipartTextMessage(phone, null, parts, null, null);
        } else {
            sm.sendTextMessage(phone, null, msg, null, null);
        }
    }

    public static void send(Context context, String phone, String msg) {
        send(phone, msg);
        SmsStorage storage = new SmsStorage(context);
        storage.add(new SmsStorage.SentMessage(phone, msg));
    }
}
