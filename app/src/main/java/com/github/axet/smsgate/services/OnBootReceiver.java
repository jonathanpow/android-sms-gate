package com.github.axet.smsgate.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.github.axet.androidlibrary.app.SuperUser;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.service.SmsBackupService;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

public class OnBootReceiver extends BroadcastReceiver {
    public static void start(Context context) {
        if (!OnExternalReceiver.mountTest(context))
            SuperUser.reboot();
        main(context);
        fragment(context);
    }

    public static void main(Context context) {
        SMSGateFragment.checkPermissions(context);
        ScheduleService.startIfEnabled(context);
        StorageReplyService.startIfEnabled(context);
        if (Build.VERSION.SDK_INT >= 18)
            NotificationListener.startIfEnabled(context);
        NotificationService.startIfEnabled(context);
    }

    public static void fragment(Context context) {
        SMSGateFragment.checkPermissions(context);
        FirebaseService.startIfEnabled(context);
        SmsReplyService.startIfEnabled(context);
        SmsBackupService.scheduleBootupBackup(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (LOCAL_LOGV) Log.v(TAG, "onReceive(" + context + "," + intent + ")");
        start(context);
    }
}
