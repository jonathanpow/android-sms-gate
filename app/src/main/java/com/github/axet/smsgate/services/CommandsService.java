package com.github.axet.smsgate.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.github.axet.androidlibrary.services.DeviceAdmin;

public class CommandsService extends IntentService {
    public static final String TAG = CommandsService.class.getSimpleName();

    public static final String REBOOT = CommandsService.class.getCanonicalName() + ".REBOOT";

    public CommandsService() {
        super(CommandsService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            Log.d(TAG, "onStartCommand " + action);
            if (action != null) {
                if (action.equals(REBOOT))
                    DeviceAdmin.reboot(this);
            }
        } else {
            Log.d(TAG, "onStartCommand restart");
        }
    }
}
