package com.github.axet.smsgate.app;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Telephony;
import android.support.v7.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;

import com.github.axet.androidlibrary.crypto.MD5;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.smsgate.providers.SMS;
import com.github.axet.smsgate.services.StorageReplyService;
import com.zegoggles.smssync.mail.PersonLookup;
import com.zegoggles.smssync.mail.PersonRecord;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Storage extends com.github.axet.androidlibrary.app.Storage {
    public static final String TAG = Storage.class.getSimpleName();

    public static final String IN = "IN";
    public static final String OUT = "OUT";
    public static final String TXT = "txt";

    public static final SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat TIME = new SimpleDateFormat("HH.mm.ss");
    public static final SimpleDateFormat SIMPLE = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
    public static final SimpleDateFormat ISO8601 = new SimpleDateFormat("yyyyMMdd\'T\'HHmmss");

    PersonLookup mPersonLookup;
    Handler handler = new Handler();
    SmsStorage storage;

    public static boolean isEnabled(Context context) {
        final Storage storage = new Storage(context);
        Uri uri = storage.getStoragePath();
        return uri != null;
    }

    public static void incoming(Context context, Runnable done) {
        if (!isEnabled(context))
            return;
        Storage storage = new Storage(context);
        storage.incoming(done);
        StorageReplyService.start(context);
    }

    public static void incoming(Context context, boolean skip) {
        if (Storage.getLastSyncDate(context) == 0 && skip)
            Storage.setLastSyncDate(context, System.currentTimeMillis());
        incoming(context, null);
    }

    public static long getLastSyncDate(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getLong(SMSApplication.STORAGE_SMS_LAST, 0);
    }

    public static void setLastSyncDate(Context context, long date) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(SMSApplication.STORAGE_SMS_LAST, date);
        edit.commit();
    }

    public static boolean filter(List<ScheduleSMS> items, SmsStorage.Message m) {
        for (ScheduleSMS item : items) {
            if (item.hide) {
                String bm = m.body.trim();
                String bi = item.message.trim();
                if (PhoneNumberUtils.compare(item.phone, m.phone) && bm.equals(bi))
                    return true;
            }
        }
        return false;
    }

    public static long filter(Context context, SmsStorage.Message m) {
        if (filter(SMSApplication.from(context).items, m))
            return m.id;
        return 0;
    }

    public static SMSMessage messageFromMap(PersonLookup contacts, Map<String, String> msgMap) {
        SMSMessage sms = new SMSMessage();

        sms.id = msgMap.get(SmsStorage.ID);
        sms.message = msgMap.get(Telephony.TextBasedSmsColumns.BODY);

        final int messageType = toInt(msgMap.get(Telephony.TextBasedSmsColumns.TYPE));

        sms.thread = msgMap.get(Telephony.TextBasedSmsColumns.THREAD_ID);

        final String address = msgMap.get(Telephony.TextBasedSmsColumns.ADDRESS);
        if (TextUtils.isEmpty(address)) {
            sms.threadPhone = "--";
            sms.threadName = "--";
        } else {
            PersonRecord record = contacts.lookupPerson(address);
            sms.threadPhone = record.getNumber();
            sms.threadName = record.getName();
        }

        if (sms.thread == null || sms.thread.isEmpty())
            sms.thread = sms.threadPhone;

        if (Telephony.TextBasedSmsColumns.MESSAGE_TYPE_INBOX == messageType) // Received message
            sms.type = IN;
        else // Sent message
            sms.type = OUT;

        long sentDate;
        try {
            sentDate = Long.valueOf(msgMap.get(Telephony.TextBasedSmsColumns.DATE));
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = System.currentTimeMillis();
        }
        sms.sent = sentDate;

        return sms;
    }

    public static SMSMessage getMessage(PersonLookup contacts, SmsStorage.Message m) {
        SMSMessage sms = new SMSMessage();

        sms.id = String.valueOf(m.id);
        sms.message = m.body;

        final int messageType = m.type;

        sms.thread = String.valueOf(m.thread);

        final String address = m.phone;
        if (TextUtils.isEmpty(address)) {
            sms.threadPhone = "--";
            sms.threadName = "--";
        } else {
            PersonRecord record = contacts.lookupPerson(address);
            sms.threadPhone = record.getNumber();
            sms.threadName = record.getName();
        }

        if (sms.thread == null || sms.thread.isEmpty() || sms.thread.equals("0") || sms.thread.equals("-1"))
            sms.thread = sms.threadPhone;

        if (Telephony.TextBasedSmsColumns.MESSAGE_TYPE_INBOX == messageType) // Received message
            sms.type = IN;
        else // Sent message
            sms.type = OUT;

        sms.sent = m.date;
        return sms;
    }

    public static String formatName(SMSMessage sms) {
        String name = ISO8601.format(sms.sent);
        if (!sms.type.isEmpty())
            name += " " + (sms.type.equals(IN) ? "↓" : "↑");
        name += " " + PhoneNumberUtils.formatNumber(sms.threadPhone);
        if (!sms.threadName.isEmpty() && !sms.threadPhone.equals(sms.threadName))
            name += " (" + sms.threadName + ")";
        name += ".txt";
        return name;
    }

    public static String formatBody(String from, SMSMessage sms) {
        String body = "(" + from + ") " + SIMPLE.format(sms.sent);
        if (!sms.type.isEmpty())
            body += " " + (sms.type.equals(IN) ? "↓" : "↑");
        body += " " + PhoneNumberUtils.formatNumber(sms.threadPhone);
        if (!sms.threadName.isEmpty() && !sms.threadPhone.equals(sms.threadName))
            body += " (" + sms.threadName + ")";
        body += "\n\n";
        body += sms.message;
        body += "\n";
        return body;
    }

    public static int toInt(String s) {
        try {
            return Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static String getFormatted(String f, SMSMessage sms) {
        f = f.replaceAll("%D", SIMPLE.format(sms.sent));
        f = f.replaceAll("%d", ISO8601.format(sms.sent));
        f = f.replaceAll("%t", sms.type.equals(IN) ? "↓" : "↑");
        f = f.replaceAll("%P", PhoneNumberUtils.formatNumber(sms.threadPhone));
        f = f.replaceAll("%p", sms.threadPhone);
        if (!sms.threadName.isEmpty() && !sms.threadPhone.equals(sms.threadName))
            f = f.replaceAll("%n", sms.threadName);
        else
            f = f.replaceAll("%n", "");
        f = f.replaceAll("%m", Build.MODEL);
        f = f.replaceAll("\\(\\)", "");
        f = f.replaceAll("  ", " ");
        f = f.trim();
        return f + "." + TXT;
    }

    public static class SMSMessage {
        public String id;
        public long sent;
        public String type; // IN/OUT
        public String thread; // thread id
        public String threadPhone; // phone
        public String threadName; // person Full Name
        public String message;

        public SMSMessage() {
        }

        public SMSMessage(long s, String type, String phone, String name, String msg) {
            this.sent = s;
            this.type = type;
            this.threadPhone = phone;
            this.threadName = name;
            this.message = msg;
        }

        public String toJSON() {
            try {
                JSONObject j = new JSONObject();
                j.put("id", id);
                j.put("sent", sent);
                j.put("type", type);
                j.put("thread", thread);
                j.put("threadPhone", threadPhone);
                j.put("threadName", threadName);
                j.put("message", message);
                return j.toString();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public long getSent() {
            return sent;
        }

        public void setSent(long sent) {
            this.sent = sent;
        }

        public String getThread() {
            return thread;
        }

        public String getThreadName() {
            return threadName;
        }

        public String getThreadPhone() {
            return threadPhone;
        }

        public void setThread(String thread) {
            this.thread = thread;
        }

        public void setThreadName(String threadName) {
            this.threadName = threadName;
        }

        public void setThreadPhone(String threadPhone) {
            this.threadPhone = threadPhone;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public Storage(Context context) {
        super(context);
        mPersonLookup = new PersonLookup(context.getContentResolver());
        storage = new SmsStorage(context);
    }

    public void incoming(final Runnable done) {
        final Runnable run = new Runnable() {
            int count = 0;
            long last = System.currentTimeMillis() - 1000; // we except message from (now - 1sec)

            @Override
            public void run() {
                count++;
                if (count > 10) {
                    if (done != null)
                        done.run();
                    return;
                }
                if (messages() < last) {  // we expect message. not yet here? retry
                    handler.postDelayed(this, 1000);
                } else {
                    if (done != null)
                        done.run();
                }
            }
        };
        run.run();
    }

    public long messages() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);

        Cursor cursor = storage.queryAfter(getLastSyncDate(context), Telephony.TextBasedSmsColumns.DATE, -1, -1);
        String from = Build.DEVICE;

        if (cursor != null) {
            int count = 0;
            while (cursor.moveToNext()) {
                count++;
                SmsStorage.Message m = SmsStorage.getMessage(cursor);
                if (Storage.filter(context, m) != 0)
                    continue;
                SMSMessage sms = getMessage(mPersonLookup, m);
                String ff = shared.getString(SMSApplication.PREF_NAMEFORMAT, "%d %t (%m) %p");
                String name = getFormatted(ff, sms);
                String text = formatBody(from, sms);
                Uri uri = getStoragePath();
                String s = uri.getScheme();
                if (s.equals(ContentResolver.SCHEME_FILE)) {
                    File file = getFile(uri);
                    File f = new File(file, name);
                    Storage.mkdirs(f.getParentFile());
                    try {
                        FileWriter out = new FileWriter(f);
                        IOUtils.write(text, out);
                        out.close();
                    } catch (IOException e) {
                        Log.e(TAG, "unable to write file", e);
                        Toast.Error(context, "unable to write file", e);
                        return 0;
                    }
                } else if (Build.VERSION.SDK_INT >= 21 && s.equals(ContentResolver.SCHEME_CONTENT)) {
                    Uri file = createFile(context, uri, name);
                    ContentResolver resolver = context.getContentResolver();
                    try {
                        OutputStream os = resolver.openOutputStream(file);
                        OutputStreamWriter out = new OutputStreamWriter(os);
                        IOUtils.write(text, out);
                        out.close();
                    } catch (IOException e) {
                        Log.e(TAG, "unable to write file", e);
                        Toast.Error(context, "unable to write file", e);
                        return 0;
                    }
                } else {
                    throw new UnknownUri();
                }
                setLastSyncDate(context, sms.sent);
            }
            cursor.close();
            if (count == 0)
                return 0;
        }

        return getLastSyncDate(context);
    }

    public Uri getStoragePath() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String path = sharedPreferences.getString(SMSApplication.PREF_STORAGE, "");
        if (path.isEmpty())
            return null;
        return super.getStoragePath(path);
    }

    @Override
    public void migrateLocalStorage() {
        migrateLocalStorage(getLocalInternal());
        migrateLocalStorage(getLocalExternal());
    }

    public void migrateLocalStorage(File file) {
        if (file == null)
            return;
        Uri uri = getStoragePath();
        String s = uri.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE) && getFile(uri).equals(file))
            return;
        File[] ff = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return getExt(pathname).toLowerCase().equals(TXT);
            }
        });
        if (ff == null)
            return;
        for (File f : ff)
            migrate(context, f, uri);
    }

    public void reply() { // allow reply using file system "file.txt -> file suffix.txt"
        migrateLocalStorage();

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);

        Cursor cursor = storage.queryAfter(0, Telephony.TextBasedSmsColumns.DATE, -1, -1);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                SmsStorage.Message m = SmsStorage.getMessage(cursor);
                if (Storage.filter(context, m) != 0)
                    continue;
                SMSMessage sms = getMessage(mPersonLookup, m);
                String nf = shared.getString(SMSApplication.PREF_NAMEFORMAT, "%d %t (%m) %p");
                String name = getFormatted(nf, sms); // file name
                Uri uri = getStoragePath();
                String s = uri.getScheme();
                if (s.equals(ContentResolver.SCHEME_FILE)) {
                    File file = getFile(uri);
                    final File f = new File(file, name);
                    final long date = f.lastModified();
                    if (f.exists()) { // if message exists
                        File parent = f.getParentFile();
                        final String left = Storage.getNameNoExt(name);
                        File[] rr = parent.listFiles(new FileFilter() {
                            @Override
                            public boolean accept(File pathname) {
                                return pathname.getName().startsWith(left) && pathname.lastModified() > date; // reply to all "file suffix.txt" and date >= original sms
                            }
                        });
                        if (rr != null && rr.length > 0) {
                            try {
                                String md5 = MD5.digest(IOUtils.toString(new FileInputStream(f), Charset.defaultCharset()));
                                for (File r : rr) {
                                    String body = IOUtils.toString(new FileInputStream(r), Charset.defaultCharset());
                                    if (body.length() > 0 && !md5.equals(MD5.digest(body))) { // content changed, then we are ready to send it
                                        SMS.send(context, m.phone, body);
                                        // Storage.touch(f);
                                        Storage.delete(r); // touch not working no every android
                                    }
                                }
                            } catch (IOException e) {
                                Log.e(TAG, "unable to read", e);
                            }
                        }
                    }
                } else if (Build.VERSION.SDK_INT >= 21 && s.equals(ContentResolver.SCHEME_CONTENT)) {
                    Uri child = Storage.getDocumentChild(context, uri, name);
                    if (Storage.exists(context, child)) {
                        final String left = Storage.getNameNoExt(name);
                        final long date = Storage.getLastModified(context, child);
                        ArrayList<Node> nn = Storage.list(context, uri, new NodeFilter() {
                            @Override
                            public boolean accept(Node n) {
                                return n.name.startsWith(left) && n.last > date;
                            }
                        });
                        if (nn.size() > 0) {
                            ContentResolver resolver = context.getContentResolver();
                            try {
                                String md5 = MD5.digest(IOUtils.toString(resolver.openInputStream(child), Charset.defaultCharset()));
                                for (Node n : nn) {
                                    String body = IOUtils.toString(resolver.openInputStream(n.uri), Charset.defaultCharset());
                                    if (body.length() > 0 && !md5.equals(MD5.digest(body))) { // content changed, then we are ready to send it
                                        SMS.send(context, m.phone, body);
                                        // Storage.touch(context, uri, name);
                                        Storage.delete(context, n.uri); // touch not working no every android
                                    }
                                }
                            } catch (IOException e) {
                                Log.e(TAG, "unable to read", e);
                            }
                        }
                    }
                } else {
                    throw new UnknownUri();
                }
            }
            cursor.close();
        }
    }
}
