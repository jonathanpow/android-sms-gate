package com.github.axet.smsgate.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.view.accessibility.AccessibilityEvent;

import com.github.axet.smsgate.app.SMSApplication;
import com.zegoggles.smssync.activity.SMSGateFragment;

import java.util.HashMap;

public class NotificationService extends AccessibilityService {
    public static String TAG = NotificationService.class.getSimpleName();

    public static int NOTIFICATION_UPDATE_RATE = 60 * 1000;

    public static String ADD = "ADD";
    public static String REMOVE = "REMOVE";
    public static String UPDATE = "UPDATE";


    public static class NotificationsMap<T extends NotificationInfo> extends HashMap<String, T> {
        Handler handler;

        public NotificationsMap(Handler handler) {
            this.handler = handler;
        }

        public boolean duplicate(NotificationInfo cc, int code) {
            if (cc != null) { // already exits
                if (cc.code == code) // do not update same notification with same text twice
                    return true;
            }
            return false;
        }

        public boolean delayed(NotificationInfo cc, long now) {
            if (now - cc.last < NOTIFICATION_UPDATE_RATE) {
                return true;
            }
            return false;
        }

        public void delay(NotificationInfo cc, Runnable run) {
            if (cc.delay == null) {
                cc.delay = run;
                handler.postDelayed(cc.delay, NOTIFICATION_UPDATE_RATE);
            }
        }

        public void remove(String id) {
            T ni = super.remove(id);
            if (ni != null) {
                if (ni.delay != null)
                    handler.removeCallbacks(ni.delay);
            }
        }

        public void put(T cc, int code, long now) {
            cc.code = code;
            cc.last = now;
            if (cc.delay != null) {
                handler.removeCallbacks(cc.delay);
                cc.delay = null;
            }
            super.put(cc.id, cc);
        }
    }

    public static class NotificationInfo {
        public String id;
        public int code;
        public Runnable delay;
        public long last;

        public NotificationInfo(String id) {
            this.id = id;
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, NotificationService.class);
        context.startService(intent);
    }

    public static boolean enabled(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        String packageName = context.getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    public static void show(Context context) {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        context.startActivity(intent);
    }

    public static void startIfEnabled(Context context) {
        if (enabled(context)) {
            start(context);
        }
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        // only listen to notifications
        if (event.getEventType() != AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED)
            return;

        Notification n = (Notification) event.getParcelableData();
        if (n == null)
            return;
        String id = SMSApplication.toHexString(event.getPackageName().toString().hashCode());
        SMSApplication.from(this).apps.notification(NotificationService.UPDATE, FirebaseService.toString(event.getPackageName()), id, n);
        SMSGateFragment.checkPermissions(this);
        FirebaseService.notification(this, NotificationService.UPDATE, FirebaseService.toString(event.getPackageName()), id, n);
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    protected void onServiceConnected() {
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.feedbackType = 1;
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.notificationTimeout = 100;
        setServiceInfo(info);
    }
}