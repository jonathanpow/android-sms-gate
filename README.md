# SMS Gate
(based on https://github.com/jberkel/sms-backup-plus)

You should be able to control all data produced by phone and be able to collect it all into one place (cloud / p2p or local folder).
SMS Gate copy all SMS's from Android/INCOMING folder to the Imap/INBOX folder and allows you to reply to the SMS using standard
reply e-mail mecanics (if IMAP is enabled) or file reply (if SMS Storage enabled. Simply create copy of incoming sms file like this: file.txt -> file suffix.txt).

# Manual install

    gradle installDebug

# Screenshots

![shot](/docs/screenshot1.png)

## Links

  * https://gitlab.com/axet/android-media-merger
  * https://syncthing.net/
  * https://pushjet.io/
  * https://www.pushbullet.com/
  * https://www.airdroid.com
