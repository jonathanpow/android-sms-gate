package smsgate

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"appengine"
	"appengine/urlfetch"
)

func init() {
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}

	var objmap map[string]interface{}
	err = json.Unmarshal(data, &objmap)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}
	if _, ok := objmap["condition"]; ok {
		fmt.Fprint(w, "!condition")
		return
	}
	if to, ok := objmap["to"]; ok {
		t := to.(string)
		if !strings.HasPrefix(t, "/topics") {
			fmt.Fprint(w, "!/topics")
			return
		}
	}

	req, err := http.NewRequest("POST", "https://fcm.googleapis.com/fcm/send", bytes.NewBuffer(data))
	req.Header.Set("Authorization", "key="+SERVER_KEY)
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Fprint(w, err)
		return
	}
	io.Copy(w, resp.Body)
	defer resp.Body.Close()
}
