var FIREBASE = require("firebase");
var config = {
  apiKey: "AIzaSyA_v_JCC_P3yyYglImXYjOiROPpqGC4zW8",
  authDomain: "android-sms-gate.firebaseapp.com",
  databaseURL: "https://android-sms-gate.firebaseio.com",
  storageBucket: "android-sms-gate.appspot.com",
};
var current; // current key for which all db subscribed (messages, shares, notifications)
var currentLast; // UI show 'new' badge
var messages; // database ref
var shares; // database ref
var notifications; //database ref
var info; //database ref
var storage; // storage ref
var sims; // SIM list (SIM1, SIM2)
var recents = []; // recent private keys {key, last, badge}
var conns = []; // active connections for each recent key
var uploads_files = []; // files to share
var uploads; // database ref

var CRYPTO = require('./crypto.js');
var UI = require('./ui.js');
var U = require('./utils.js');
var UUID = require('node-uuid');

global.messages = function messages() {
  if(!pub()) {
    return;
  }
  command({
    command: "refresh"
  });
}

global.reboot = function reboot() {
  if(!pub()) {
    return;
  }
  command({
    command: "reboot"
  });
}

global.wipe = function reboot() {
  if(!pub()) {
    return;
  }
  command({
    command: "wipe"
  });
}

global.lock = function reboot() {
  if(!pub()) {
    return;
  }
  command({
    command: "lock"
  });
}

global.share = function share(cmd) {
  if(!pub()) {
    return;
  }
  cmd.command = "share";
  command(cmd);
}

global.notificationCancel = function notificationCancel(val) {
  command({
    command : "notificationcancel",
    id: val
  });
}

global.send = function send(sim, phone, id) {
  var text = $('#' + id).val();
  $('#' + id).val('');

  if (!phone || !text) {
    return
  }
  
  command({
    command: "send",
    sim: sim,
    phone: phone,
    message: text
  });
}

global.deleteall = function deleteall(thread) {
  command({
    command: "deleteall",
    thread: thread
  });
}

function command(cmd) {
  var url = "https://us-central1-android-sms-gate.cloudfunctions.net/topics";
  var ref = CRYPTO.address(current.key).toString();
  var json = {
    "topic" : ref,
    "data": {
      "text": CRYPTO.encrypt(current.key, new Buffer(JSON.stringify(cmd))).toString('base64')
    }
  }
  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, true)
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.onreadystatechange = function() {
    if (this.readyState != 4)
      return;
  };
  xhr.send(JSON.stringify(json));
}

function recentsLoad() {
  var secrets = $('#secrets');
  secrets.listview();
  secrets.empty();
  if (recents.length == 0) {
    var item = $('<li><div href="#" class="listitem" align="center"><i>empty</i></div></li>');
    secrets.append(item);
  }
  var badge_total = 0;
  for(var i = 0; i < recents.length; i++) {
    var rec = recents[i];
    badge_total += rec.badge;
    var badge = $('<span id="badge_' + i + '" class="badge">' + rec.badge + '</span>');
    if (rec.badge > 0) {
      badge.show();
    } else {
      badge.hide();
    }
    var item = $('<li></li>');
    var div = $('<div class="ui-controlgroup-controls ">')
    var key = rec.key;
    if(rec.name) {
      key = rec.name;
    }
    var sec = $('<a href="#" class="ui-btn ui-corner-all controlgroup-text">' + key + '</a>');
    (function(rec) { // select secret by click
      sec.click(function() {
        if (rec.name) {
          $('#secret').val(rec.name);
        }else {
          $('#secret').val(rec.key);
        }
        pub();
        $('#popupSecrets').popup('close');
      });
    })(rec);
    sec.append(badge);
    div.append(sec);
    var del = $('<a class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext">Delete</a>');
    (function(i) { // remove secret by Delete click
      del.click(function() {
        recents.splice(i, 1);
        recentsLoad();
        save();
      });
    })(i);
    var edit = $('<a href="#" data-rel="popup" data-transition="fade" class="ui-btn ui-corner-all ui-icon-edit ui-btn-icon-notext">Edit</a>');
    (function(rec) { // edit button click
      edit.click(function() {
        var edit = function() {
          $('#popupSecrets').off('popupafterclose', edit);
          var d = $('#editDialog');
          d.find('#device_name').val(rec.name);
          d.find('#device_key').val(rec.key);
          d.find('#editSave').off('click');
          d.find('#editSave').click(function() {
            rec.name = d.find('#device_name').val();
            if (current && current.key == rec.key) {
              if (rec.name) {
                $('#secret').val(rec.name);
              }else {
                $('#secret').val(rec.key);
              }
            }
            recentsLoad();
            save();
            d.popup('close');
          });
          d.popup('open');
        };
        $('#popupSecrets').on('popupafterclose', edit).popup('close');
      });
    })(rec);
    div.append(edit);
    div.append(del);
    item.append(div);
    secrets.append(item);
  }
  secrets.listview("refresh");
  if (badge_total > 0) {
    $('#badge_total').text(badge_total);
    $('#badge_total').show();
  }else {
    $('#badge_total').hide();
  }
}

function recentsConnect() {  
  for( var i in conns) {
    var conn = conns[i];
    if (conn.messages) {
      conn.messages.off('child_added', conn.messages.callback_add);
    }
    if (conn.shares) {
      conn.shares.off('child_added', conn.shares.callback_add);
    };
    if (conn.notifications) {
      conn.notifications.off('child_added', conn.notifications.callback_add);
    }
  }
  conns = [];
  var key = current.key;
  for(var i in recents) {
    var rec = recents[i];
    if(rec.key == key) {
      continue;
    }
    var conn = {};
    conn.rec = rec;
    conn.last = rec.last;
    if (rec.toast > conn.last) {
      conn.last = rec.toast;
    }
    conn.last += 1;
    var a = CRYPTO.address(rec.key);
    var ref = '/users/' + a + '/messages';
    conn.messages = FIREBASE.database().ref(ref);
    conn.messages.orderByChild('date').startAt(conn.last).on("child_added", conn.messages.callback_add = function(conn) {
      return function(snapshot) {
        var enc = new Buffer(snapshot.val().text, 'base64');
        var bin = CRYPTO.decrypt(conn.rec.key, enc);
        var text = bin.toString();
        var obj = JSON.parse(text);
        conn.rec.badge += 1;
        badgeUpdate(conn.rec);
        UI.toast("New Message " + (conn.rec.name ? "'" + conn.rec.name + "'" : "") + " from: " + obj.threadName);
        if (!conn.rec.toast || snapshot.val().date > conn.rec.toast) {
          conn.rec.toast = snapshot.val().date;
        }
      }
    }(conn));
    var ref = '/users/' + a + '/shares';
    conn.shares = FIREBASE.database().ref(ref);
    conn.shares.orderByChild('date').startAt(conn.last).on("child_added", conn.shares.callback_add = function(conn) {
      return function(snapshot) {
        var enc = new Buffer(snapshot.val().text, 'base64');
        var bin = CRYPTO.decrypt(conn.rec.key, enc);
        var text = bin.toString();
        conn.rec.badge += 1;
        badgeUpdate(conn.rec);
        UI.toast("New Share " + (conn.rec.name ? "'" + conn.rec.name + "'" : ""));
        if (!conn.rec.toast || snapshot.val().date > conn.rec.toast) {
          conn.rec.toast = snapshot.val().date;
        }
      }
    }(conn));
    var ref = '/users/' + a + '/notifications';
    conn.notifications = FIREBASE.database().ref(ref);
    conn.notifications.orderByChild('date').startAt(conn.last).on("child_added", conn.notifications.callback_add = function(conn) {
      return function(snapshot) {
        var enc = new Buffer(snapshot.val().text, 'base64');
        var bin = CRYPTO.decrypt(conn.rec.key, enc);
        var text = bin.toString();
        var obj = JSON.parse(text);
        conn.rec.badge += 1;
        badgeUpdate(conn.rec);
        UI.toast("New Notification " + (conn.rec.name ? "'" + conn.rec.name + "'" : ""));
        if (!conn.rec.toast || snapshot.val().date > conn.rec.toast) {
          conn.rec.toast = snapshot.val().date;
        }
      }
    }(conn));
    conns.push(conn);
  }
}

function recentsLast(date) {
  if (date > current.last) {
    current.last = date;
  }
}

function recentsFind(obj) {
  for(var i in recents) {
    var rec = recents[i];
    if (rec.key == obj || rec.name == obj) {
      return rec;
    }
  }
  return null;
}

// save current key to recents
function recentsSave(key) {
  for(var i in recents) {
    var rec = recents[i];
    if (rec.key == key) {
      rec.badge = 0;
      badgeUpdate(rec);
      return rec;
    }
  }
  var i = recents.push({
    key: key,
    last: 0, // last loaded event
    toast: 0, // last loaded event for toasts
    badge: 0,
  });
  recentsLoad();
  return recents[i - 1];
}

function badgeUpdate(rec) {
  var badge_total = 0;
  for(var i in recents) {
    var r = recents[i];
    if (r.key == rec.key) {
      if (rec.badge > 0) {
        $('#badge_' + i).show().text(rec.badge);
      } else {
        $('#badge_' + i).hide();
      }
    }
    badge_total += r.badge;
  }
  if(badge_total > 0) {
    $('#badge_total').text(badge_total).show();
  } else {
    $('#badge_total').hide();
  }
}

function simsLoad() {
  var options = $('.sims');
  options.empty();
  for(var i in sims) {
    var s = sims[i];
    options.append($("<option />").attr('value', i).text(s));
  }
  if (sims.length == 1) { // "Default"
    options.addClass('ui-disabled');
  } else {
    options.removeClass('ui-disabled');
  }
  options.trigger('change');
}

function uploads_select(files) {
  for (var i = 0, f; f = files[i]; i++) {
    uploads_files.push(f);
  }
  var list = $('#uploads_files').listview();
  list.empty();
  for(var i in uploads_files) {
    var f = uploads_files[i];
    var del = $('<a href="#">Delete</a>');
    var title = $('<a href="#" class="listitem">' + escape(f.name) + '<p>' + U.formatSize(f.size) + '</p></a>');
    var item = $('<li></li>');
    (function(i, item) {
      del.click(function() {
        uploads_files.splice(i, 1)
        item.remove();
      });
    })(i, item);
    var progress = $('<div id="uploads_progress_' + i + '" class="progress"><div class="progress_bar"></div></div>');
    title.append(progress);
    item.append(title);
    item.append(del);
    list.append(item);
  }
  list.listview('refresh');
}

function uploads_next(i, up, t) {
  var progress = $('#uploads_progress_' + i);
  var reader = new FileReader();
  var file = uploads_files[i];
  reader.onloadend = (function(bar, file) {
    return function(e) {
      if(e.target.readyState == FileReader.DONE) {
        var b = new Buffer(e.target.result);
        var buf = CRYPTO.encrypt(current.key, b);
        var task = storage.child(up.key).child(UUID.v1()).put(buf);
        task.on('state_changed', function(snapshot){
          var p = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          bar.width(p + '%');
        });
        task.then(t);
      }
    }
  })(progress.find('.progress_bar'), file);
  reader.readAsArrayBuffer(file);
}

function uploads_init() {
  var file = $('input:file');
  var area = $('.uploads_area');
  area.click(function(e) {
    if(file.e)
      return;
    file.e = e;
    file.val('');
    file.click();
    file.e = null;
  });
  file.on('change', function(e) {
    uploads_select(e.target.files);
  });
  area.on('dragover', function(e) {
    e.stopPropagation();
    e.preventDefault();
    e.originalEvent.dataTransfer.dropEffect = 'copy';
  });
  area.on('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();
    uploads_select(e.originalEvent.dataTransfer.files);
  });
  $('#uploads_ok').click(function() {
    var text = $('#uploads_text').val();
    if(!text && uploads_files.length == 0) {
      console.log('bad')
      return;
    }
    $(this).addClass('ui-disabled');
    if(uploads_files.length != 0) {
      console.log(uploads_files.length)
      var uris = [];
      var up = uploads.push();
      var i = 0;
      var next = function(snapshot) {
        var file = uploads_files[i];
        uris.push({
          "uri": snapshot.downloadURL,
          "filename": file.name,
          "filesize": file.size,
          "mimetype": file.type
        });
        if(uris.length == uploads_files.length) {
          var cmd = {
            "text": text,
            "uris": uris
          };
          up.set({
            "date": new Date().getTime(),
            "text": CRYPTO.encrypt(current.key, new Buffer(JSON.stringify(cmd))).toString('base64')
          });
          share({
            "text": text,
            "uploads": up.key,
            "uris": uris
          });
          $('#uploads_text').val('');
          $('#uploadsDialog').popup('close');
          UI.toast("Shared!");
        } else {
          i++;
          uploads_next(i, up,  next);
        }
      }
      uploads_next(i, up, next);
    } else {
      console.log('text')
      share({
        "text": text
      });
      $('#uploads_text').val('');
      $('#uploadsDialog').popup('close');
      UI.toast("Shared!");
    }
  });
  $('[href=#uploadsDialog]').click(function() {
    $('#uploads_files').listview().empty();
    uploads_files = [];
    $('#uploads_ok').removeClass('ui-disabled');
    $('#uploadsDialog').popup({
      afteropen: function(){
        $('#uploads_text').focus();
      }
    });
  });
}

function init() {
  FIREBASE.initializeApp(config);

  FIREBASE.auth().onAuthStateChanged(function(user) {
  });

  if (!FIREBASE.auth().currentUser) {
    FIREBASE.auth().signInWithEmailAndPassword("anonymous@anonymous.com", "anonymous").catch(function(error) {
      console.error(error);
    });
  }
  
  pub();

  var badge = $('<span id="badge_total" class="badge"></span>').hide();
  $('[href=#popupSecrets]').append(badge).click(function() {
    pub();
  });
  $('#shares_header').hide();
  $('#notifications_header').hide();
  $('.sims').addClass("ui-disabled");

  uploads_init();

  recentsLoad();
}

// messages listview

function messages_append(obj) {
  var thread = 'thread_' + obj.thread;
  var messages = 'messages_' + obj.thread;
  var last = 'last_' + obj.thread;
  var reply = 'reply_' + obj.thread;
  var thread_title = 'thread_title_' + obj.thread;
  var deleteall = 'deleteall_' + obj.thread;
  if ($('#' + thread).length == 0) {
    var item = $('<div id="' + thread + '" data-role="collapsible"><h2 id="' + thread_title + '">' + obj.threadName + '</h2><ul id="' + messages + '" data-role="listview" data-split-icon="delete" data-split-theme="none">');
    UI.fade(item);
    $('#messages_list').append(item);
    var item = $('<li id="' + last + '"><div class="ui-field-contain"><label for="text">Reply:</label><textarea name="text" id="' + reply + '"></textarea></div></li>');
    var div = $('<div></div');
    var delall = $('<a href="#" data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop">Delete All</a>');
    delall.click(function(){ // open 'delete all' dialog
      $('#deleteallok').off('click');
      $('#deleteallok').click(function() {
        global.deleteall(obj.thread);
      });
      $('#deleteAllDialog').popup('open');
    });
    var submit = $('<input class="ui-btn-right" type="submit" data-inline="true" value="Submit">');
    submit.click(function() {
      send(0, obj.threadPhone, reply);
    });
    div.append(delall);
    div.append(submit);
    item.append(div);
    $('#' + messages).append(item);
    $('#messages_list').trigger("create");
  }
  var a;
  var b;
  if (obj.type == "IN") {
    a = "left";
    b = "ui-btn-icon-left";
  } else {
    a = "right";
    b = "ui-btn-icon-right";
  }
  var message = 'message_' + obj.thread + '_' + obj.id;
  var msg = $('<div class="ui-icon-user listitem ' + b + '" align="' + a + '">' + obj.message + '</div>');
  if (obj.date > currentLast) {
    var badge = $('<span class="badge">New</span>');
    msg.append(badge);
    var title = $('#' + thread_title);
    if (title.find(".badge").length == 0) {
      title.append(badge);
    }
  }
  var t = new Date(obj.sent);
  var tt = U.formatDate(t) + ' ';
  var prev = $('#' + last).prev();
  if (prev.length) {
    var ps = new Date(parseInt(prev.attr('sent')));
    if (ps.toDateString() === t.toDateString()) {
      tt = "";
    }
  }
  tt += U.formatTime(t);
  var time = $('<div class="listitem ' + b + '" align="' + a + '"><p>' + tt + '</p></div>');
  var item = $('<li id="' + message + '" sent=' + obj.sent + '></li>');
  item.append(msg);
  item.append(time);
  if ($('#' + message).length == 0) {
    $('#' + last).before(item);
    $('#' + messages).listview("refresh");
    return true;
  } else {
    item.fadeOut(400);
    item.fadeIn(400);
    $('#' + message).replaceWith(item);
    $('#' + messages).listview("refresh");
  }
  return false;
}

function messages_remove(obj) {
  var thread = 'thread_' + obj.thread;
  var messages = 'messages_' + obj.thread;
  var last = 'last_' + obj.thread;
  var reply = 'reply_' + obj.thread;
  var message = 'message_' + obj.thread + '_' + obj.id;
  if ($('#' + message).length != 0) {
    $('#' + message).remove();
    if($('#' + messages).children().length == 1) { // reply form last element
      $('#' + thread).remove();
    }
  }
}

// shares listview

function shares_file(obj) {
  var file = $('<div class="ui-btn dn-btn"></div>')
  file.text(obj.filename);
  file.click(function() {
    // prevent outer div click
    var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    var xhr = new XMLHttpRequest();
    xhr.open('GET', obj.uri, true);
    xhr.responseType = 'arraybuffer';
    xhr.onprogress = function(event) {
    };
    xhr.onload = function(e) {
      if (this.status == 200) {
        var buf = new Buffer(this.response);
        var data = CRYPTO.decrypt(current.key, buf);
        var a = window.document.createElement('a');
        a.href = window.URL.createObjectURL(new Blob([data], { type: obj.mimetype }));
        a.download = obj.filename;
        document.body.appendChild(a)
        a.click();
        document.body.removeChild(a)
      }
    };
    xhr.send();
  });
  return file;
}

function shares_img(obj) {
  var img = $('<img class="viewbox-image"></img>');
  img.on('viewbox.open', function(event) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', obj.uri, true);
    xhr.responseType = 'arraybuffer';
    xhr.onprogress = function(event) {
    };
    xhr.onload = function(e) {
      if (this.status == 200) {
        var buf = new Buffer(this.response);
        var data = CRYPTO.decrypt(current.key, buf);
        img.attr('src', 'data:image/png;base64,' + data.toString('base64'));
      }
    };
    xhr.send();
  });
  return img;
}

function shares_append(obj) {
  var share = 'share_' + obj.key;
  if ($('#shares_list').children().length == 0) {
    UI.fade($('#shares_header'));
    $('#shares_list').show();
  }
  if ($('#' + share).length == 0) {
    var text = obj.text || "";
    var title = $('<a>' + text + '</a>');
    if (obj.date > currentLast) {
      var badge = $('<span class="badge">New</span>');
      title.append(badge);
    }
    if(obj.preview) {
      var img = $('<img class="ui-li-thumb"></img>');
      img.attr('src', 'data:image/png;base64,' + obj.preview);
      title.append(img);
      title.click(function() {
        var imgs = [];
        if (obj.filename) {
          var img = shares_img(obj);
          imgs.push(img);
        }
        if (obj.uri instanceof Array) {
          for (var i in obj.uri) {
            var uri = obj.uri[i];
            if(uri.mimetype.startsWith('image/')) {
              var img = shares_img(uri);
              imgs.push(img)
            }
          }
        }
        var c = $($.map(imgs, function(e){return e.get();}));
        var vb = c.viewbox();
        vb.trigger('viewbox.open', 0);
      });
    } else {
      title.addClass('listitem');
    }
    
    if (obj.filename) {
      var file = shares_file(obj);
      title.append(file)
    }
    
    if (obj.uri instanceof Array) {
      for (var i in obj.uri) {
        var uri = obj.uri[i];
        var file = shares_file(uri);
        title.append(file)
      }
    }

    var removeUri = function(uri) {
      var ref = FIREBASE.storage().refFromURL(uri);
      ref.delete().catch(function(error) {
        console.log(error);
      });
    }

    var del = $('<a href="#">Delete</a>');
    del.click(function() {
      if (obj.uri instanceof Array) {
        for(var i in obj.uri) {
          var uri = obj.uri[i].uri;
          removeUri(uri);
        }
      }
      if(typeof obj.uri == "string") {
        removeUri(obj.uri);
      }
      shares.child(obj.key).remove();
    });
    var item = $('<li id="' + share + '"></li>');
    item.append(title);
    item.append(del);
    UI.fade(item);
    $('#shares_list').append(item);
    $('#shares_list').listview("refresh");
    return true;
  }
  return false;
}

function shares_remove(id) {
  var share = 'share_' + id;
  if ($('#' + share).length != 0) {
    $('#' + share).fadeOut(400, function() {
      $(this).remove();
      if($('#shares_list').children().length == 0) {
        $('#shares_header').hide();
        $('#shares_list').hide();
      }
    });
  }
}

// notifications listview

function notifications_append(obj) {
  var notification = 'notification_' + obj.key;
  if ($('#notifications_list').children().length == 0) {
    UI.fade($('#notifications_header'));
    $('#notifications_list').show();
  }
  var msg = "";
  if(obj.text) {
    var mm = obj.text.split(/\n/);
    for(var i in mm) {
      msg += '<p class="listitem">' + mm[i] + '</p>';
    }
  }
  var b = "";
  if(!obj.details) {
    b = "listitem";
  }
  var title = $('<a class="' + b + '"><h3>' + obj.title + '</h3>' + msg + '</a>');
  if (obj.date > currentLast) {
    var badge = $('<span class="badge">New</span>');
    title.append(badge);
  }
  var del = $('<a href="#">Delete</a>');
  del.click(function() {
    notificationCancel(obj.key);
  });
  var item = $('<li id="' + notification + '"></li>');
  item.append(title);
  item.append(del);
  if(obj.details) {
    title.click(function() {
      var msg = "";
      var mm = obj.details.split(/\n/);
      for(var i in mm) {
        msg += '<p class="listitem">' + mm[i] + '</p>';
      }
      $('#notificationTitle').text(obj.title);
      $('#notificationDetails').html(msg);
      $('#notificationDialog').popup('open');
    });
  }
  if ($('#' + notification).length == 0) {
    UI.fade(item);
    $('#notifications_list').append(item);
    $('#notifications_list').listview("refresh");
    return true;
  } else {
    item.fadeOut(400);
    item.fadeIn(400);
    $('#' + notification).replaceWith(item);
    $('#notifications_list').listview("refresh");
  }
  return false;
}

function notifications_remove(id) {
  var notification = 'notification_' + id;
  if ($('#' + notification).length != 0) {
    $('#' + notification).fadeOut(400, function() {
      $(this).remove();
      if($('#notifications_list').children().length == 0) {
        $('#notifications_header').hide();
        $('#notifications_list').hide();
      }
    });
  }
}

// subscribe to new key address
function pub() {
  var key = $('#secret').val();

  current = recentsFind(key);

  var a;
  if (!current) {
    a = CRYPTO.address(key);
    if (!a) {
      return false;
    }
    current = recentsSave(key);
  } else {
    current.badge = 0;
    badgeUpdate(current);
    a = CRYPTO.address(current.key);
  }

  currentLast = current.last;

  recentsConnect();

  var ref = '/users/' + a + '/messages';
  if (messages) { // delete old messages, since private key changed.
    if (messages.path != ref) {
      $('#messages_list').empty();
    }
    $('#messages_list').find('.badge').remove(); // remove 'new' from threads
    messages.off('child_added', messages.callback_add);
    messages.off('child_changed', messages.callback_change);
    messages.off('child_removed', messages.callback_removed);
  }
  messages = FIREBASE.database().ref(ref);
  messages.on("child_added", messages.callback_add = function(snapshot) {
    var enc = new Buffer(snapshot.val().text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);
    obj.key = snapshot.key;
    obj.date = snapshot.val().date;
    messages_append(obj);
    recentsLast(snapshot.val().date);
  });
  messages.on("child_changed", messages.callback_change = function(snapshot) {
    var enc = new Buffer(snapshot.val().text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);
    obj.key = snapshot.key;
    obj.date = snapshot.val().date;
    messages_append(obj);
    recentsLast(snapshot.val().date);
  });
  messages.on("child_removed", messages.callback_removed = function(snapshot) {
    var enc = new Buffer(snapshot.val().text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);
    messages_remove(obj);
  });
  
  var ref = '/users/' + a + '/shares';
  if (shares) { // delete old shares, since private key changed.
    if (shares.path != ref) {
      $('#shares_header').hide();
      $('#shares_list').empty();
    }
    shares.off('child_added', shares.callback_added);
    shares.off('child_removed', shares.callback_removed);
  }
  shares = FIREBASE.database().ref(ref);
  shares.orderByChild("date").on("child_added", shares.callback_added = function(snapshot) {
    var enc = new Buffer(snapshot.val().text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);
    obj.key = snapshot.key;
    obj.date = snapshot.val().date;
    shares_append(obj);
    recentsLast(snapshot.val().date);
  });
  shares.on("child_removed", shares.callback_removed = function(snapshot) {
    shares_remove(snapshot.key);
  });

  var ref = '/users/' + a + '/notifications';
  if (notifications) { // delete old notifications, since private key changed.
    if (notifications.path != ref) {
      $('#notifications_header').hide();
      $('#notifications_list').empty();
    }
    notifications.off('child_added', notifications.callback_add);
    notifications.off('child_changed', notifications.callback_change);
    notifications.off('child_removed', notifications.callback_remove);
  }
  notifications = FIREBASE.database().ref(ref);
  notifications.orderByChild("date").on("child_added", notifications.callback_add = function(snapshot) {
    var enc = new Buffer(snapshot.val().text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);
    obj.key = snapshot.key;
    obj.date = snapshot.val().date;
    notifications_append(obj);
    recentsLast(snapshot.val().date);
  });
  notifications.on("child_changed", notifications.callback_change = function(snapshot) {
    var enc = new Buffer(snapshot.val().text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);
    obj.key = snapshot.key;
    obj.date = snapshot.val().date;
    notifications_append(obj);
    recentsLast(snapshot.val().date);
  });
  notifications.on("child_removed", notifications.callback_remove = function(snapshot) {
    notifications_remove(snapshot.key);
  });

  var ref = '/users/' + a + '/info';
  if (info) { // clear old info, since private key changed.
    if (info.path != ref) {
      $('#info').val('');
    }
    info.off('value', info.callback_value);
  }
  info = FIREBASE.database().ref(ref);
  info.on("value", info.callback_value = function(snapshot) {
    var val = snapshot.val();
    if (!val) {
      return;
    }
    var enc = new Buffer(val.text, 'base64');
    var bin = CRYPTO.decrypt(current.key, enc);
    var text = bin.toString();
    var obj = JSON.parse(text);

    time = U.formatDuration(obj.uptime);

    $('#info').text('Uptime: ' + time + (obj.signal && ', ' + obj.signal || "") + ', OS: ' + obj.os + ', ' + obj.battery);

    sims = [];
    sims.push("Default");
    for (var i = 0; i < obj.sim; i++) {
      sims.push("SIM" + (i + 1));
    }
    simsLoad();
  });
  
  var ref = '/users/' + a;
  storage = FIREBASE.storage().ref().child(ref);
  var ref = '/users/' + a + '/uploads';
  uploads = FIREBASE.database().ref(ref);
  return true;
}

function save() {
  localStorage.setItem('secret', $('#secret').val());
  localStorage.setItem('phone', $('#phone').val());
  localStorage.setItem('sms', $('#sms').val());
  var rec = JSON.stringify(recents)
  rec && localStorage.setItem('recents', rec);
}

function save_item(k, v) {
  localStorage.setItem(k, v)
}

function load() {
  $('#secret').val(localStorage.getItem('secret'));
  $('#phone').val(localStorage.getItem('phone'));
  $('#sms').val(localStorage.getItem('sms'));
  recents = JSON.parse(localStorage.getItem('recents')) || [];
}

window.onbeforeunload = function() {
  save();
}

window.onload = function() {
  load();
  init();
}
